<?php
session_start();
if(!$_SESSION['email']){
    header("location: login.php");
}
else{
    $email = $_SESSION['email'];
    $url = 'http://localhost/sum/api/fetchUserId.php';
    $data = array('email' => $email);

// use key 'http' even if you send the request to https://...
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);
    if ($result === FALSE) {
        /* Handle error */
        echo  "Retry Again";}
//    echo $result;
    $id = $result;



}
?>
<html>
<head>
    <title>The Two Frogs</title>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: 'Open Sans', sans-serif;
            margin: 0;
            padding: 0;
            /*background: #F0F3F6;*/
            /*overflow: hidden;*/
        }

        tr:nth-child(even) {
            background-color: #dddddd;

        }

    </style>
</head>
<body >

<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="../index.php">Stories</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="../logout.php">Logout</a>
        </li>
    </ul>
</nav>

<br><br><br><br><br><br>
<div class="container texting">
    <div class="jumbotron">
        <h1>Bootstrap Tutorial</h1>
    </div>
    <p>This is some text. This is some text. This is some text. This is some text. This is some text.</p>
</div>
<br>
<br>
<div class="container count">
    <div class="container mt-3 .align-content-center text-white">

        <div class="d-flex mb-3">
            <div class="p-4 flex-fill bg-info "><center>read count 1</center></div>

            <div class="p-4 flex-fill bg-primary"><center>live count 1</center></div>
        </div>


    </div>
</div>

<script>
    function myFunction() {
        // return "Write something clever here...";
        alert("woow");
    }
    window.addEventListener("beforeunload", function(event) {
        console.log("hhhhh");
        var userId = '<?php echo $id;?>';
        var sId = 2;
        $.ajax({
            url: '../api/live.php',
            type: 'POST',
            data:{
                sId:sId,
                userId:userId
            },
            success: function (result){
                console.log("reaching")

            }
        });
        event.returnValue = "Write something clever here..";
    });
</script>


<script>
    $(document).ready(function (){
        // fetchName();
        // fetchSum();
        liveAgain();
        fetchContent();
        fetchCount();

        setInterval(fetchCount,5000);
    });


    function liveAgain() {
        var userId = '<?php echo $id;?>';
        var sId = 2;
        $.ajax({
            url: '../api/liveAgain.php',
            type: 'POST',
            data:{
                sId:sId,
                userId:userId
            },
            success: function (result){
                console.log("reaching")

            }
        });
    }

    function fetchContent() {
        var userId = '<?php echo $id;?>';
        var sId = 2;
        $.ajax({
            url: '../api/content.php',
            type: 'POST',
            data:{
                sId:sId,
                userId:userId
            },
            success: function (result){
                console.log("reaching")
                $(".texting").html(result);
            }
        });
    }

    function fetchCount() {
        var sId = 2;
        $.ajax({
            url: '../api/count.php',
            type: 'POST',
            data:{
                sId:sId
            },
            success: function (result){
                console.log("reaching")
                $(".count").html(result);
            }
        });
    }


    function fetchSum() {
        $.ajax({
            url: 'api/sum.php',
            type: 'POST',
            success: function (result){
                console.log("reaching")
                $(".detail").html(result);
            }
        });
    }

    function fetchName(){
        $.ajax({
            url: 'api/fetchAccount.php',
            type: 'POST',
            success: function (result){
                console.log("reaching")
                $(".sum").html(result);
            }
        });
    }
</script>

</body>

</html>