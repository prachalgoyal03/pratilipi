<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/sum/config/config.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/sum/api/DAO/generalDAO.php';

//having post variable from  story page
$sId = $_POST['sId'];
//$params['sId'] = $sId;
$userId = $_POST['userId'];
//$params['userId'] = $userId;

//checking if in read table that user have read this before or not.
$data1 = generalDAO::getSQLResult("SELECT COUNT(*) AS num FROM `read` WHERE userId = '$userId' and sId = '$sId'");

//if user have not read insering user to the read table to increase read count
if ($data1[0]['num'] == 0) {
//    $result = generalDAO::addOrder($params);
//    $sql="INSERT INTO read (sId, userId) VALUES ('$sId','$userId')";
    global $conn;
    $query = "INSERT INTO story.read SET sId = ?, userId = ?";

    $stmt = $conn->prepare($query);

    $stmt->bindParam(1, $sId);
    $stmt->bindParam(2, $userId);

    // execute the query
    if($stmt->execute()){
//        echo "<div>Successful registration.</div>";

    }else{
        echo "<div>Unable to register. <a href='register.php'>Please try again.</a></div>";
    }
}

//fetching title and content of the story
$data = generalDAO::getSQLResult("select title,content from page where sId = '$sId'");


$text ="";
$text = "<div class=\"jumbotron\">
        <h1>".$data[0]['title']."</h1>
    </div>
    <p>".$data[0]['content']."</p>";

echo $text;

