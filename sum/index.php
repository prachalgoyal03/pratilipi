<?php
session_start();
if(!$_SESSION['email']){
    header("location: login.php");
}

?>
<html>
<head>
    <title>stories</title>
    <meta charset="utf-8">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: 'Open Sans', sans-serif;
            margin: 0;
            padding: 0;
            background: #F0F3F6;
            /*overflow: hidden;*/
        }

        tr:nth-child(even) {
            background-color: #dddddd;

        }

    </style>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
    <a class="navbar-brand" href="index.php">Stories</a>
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="logout.php">Logout</a>
        </li>
    </ul>
</nav>

<br><br><br><br><br><br>
<div class="container">
    <ul class="list-group">

        <li class="list-group-item list-group-item-dark">Dark item<span class="badge badge-primary badge-pill">12</span></li>
        <li class="list-group-item list-group-item-light">Light item</li>
        <li class="list-group-item list-group-item-dark">Dark item<span class="badge badge-primary badge-pill">12</span></li>
        <li class="list-group-item list-group-item-light">Light item</li>
        <li class="list-group-item list-group-item-dark">Dark item<span class="badge badge-primary badge-pill">12</span></li>
        <li class="list-group-item list-group-item-light">Light item</li>
        <li class="list-group-item list-group-item-dark">Dark item<span class="badge badge-primary badge-pill">12</span></li>
        <li class="list-group-item list-group-item-light">Light item</li>
    </ul>
</div>


<script>
    $(document).ready(function (){

        fetchTitle();
    });

    function fetchTitle() {
        $.ajax({
            url: 'api/title.php',
            type: 'POST',
            success: function (result){
                console.log("reaching")
                $(".list-group").html(result);
            }
        });
    }

</script>
</body>

</html>