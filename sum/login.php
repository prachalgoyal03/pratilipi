<?php
//Chitvan_12345
session_start();
if($_SESSION['email']) {
    header("location: index.php");
}
?>
<html>
<head>
    <title>login page </title>
    <link type="text/css" rel="stylesheet" href="css/style.css" />
</head>
<body>

<div id="loginForm">

    <?php
    // form is submitted, check if acess will be granted
    if($_POST){

        try{
            // load database connection and password hasher library
            require 'config/config.php';
            require '../salt.php';
//            require 'lib/PasswordHash.php';

            // prepare query
            $query = "select email, password from user where email = ? limit 0,1";
            $stmt = $conn->prepare( $query );

            // this will represent the first question mark
            $stmt->bindParam(1, $_POST['email']);

            // execute our query
            $stmt->execute();

            // count the rows returned
            $num = $stmt->rowCount();

            if($num==1){

                //store retrieved row to a 'row' variable
                $row = $stmt->fetch(PDO::FETCH_ASSOC);

                // hashed password saved in the database
                $storedPassword = $row['password'];


                $postedPassword = $_POST['password'];
                $salt = sha1(md5($postedPassword)).$crypt;
                $postedPassword = md5($postedPassword.$salt);

                if($postedPassword == $storedPassword){
                    echo "<div>Access granted.</div>";
                    $_SESSION['email']=$_POST['email'];
                    echo "<script>window.open('index.php','_self')</script>";
                }

                // $check variable is false, access denied.
                else{
                    echo "<div>Access denied. <a href='login.php'>Back.</a></div>";
                }

            }

            // no rows returned, access denied
            else{
                echo "<div>Access denied. <a href='login.php'>Back.</a></div>";
            }

        }
            //to handle error
        catch(PDOException $exception){
            echo "Error: " . $exception->getMessage();
        }


    }
    else{
        ?>

        <form action="login.php" method="post">

            <div id="formHeader">Website Login</div>

            <div id="formBody">
                <div class="formField">
                    <input type="email" name="email" required placeholder="Email" />
                </div>

                <div class="formField">
                    <input type="password" name="password" required placeholder="Password" />
                </div>

                <div>
                    <input type="submit" value="Login" class="customButton" />
                </div>
            </div>
            <div id='userNotes'>
                New here? <a href='register.php'>Register for free</a>
            </div>
        </form>

        <?php
    }
    ?>

</div>

</body>
</html>